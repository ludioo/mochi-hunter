﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    public GameObject mainMenuHolder;
    public GameObject optionMenuHolder;
    public GameObject creditMenuHolder;
    public GameObject chooseLevelMenuHolder;
    public Slider[] volumeSliders;
    public Toggle[] resolutionToggles;
    public Toggle fullScreenToggle;
    public int[] screenWidths;
    int activeScreenResIndex;

    private void Start()
    {
        activeScreenResIndex = PlayerPrefs.GetInt("Screen res index");
        bool isFullScreen = (PlayerPrefs.GetInt("FullScreen") == 1) ? true : false;

        volumeSliders[0].value = AudioManager.instance.masterVolumePercent;
        volumeSliders[1].value = AudioManager.instance.musicVolumePercent;
        volumeSliders[2].value = AudioManager.instance.sfxVolumePercent;

        for(int i = 0;i < resolutionToggles.Length;i++)
        {
            resolutionToggles[i].isOn = i == activeScreenResIndex;
        }
        fullScreenToggle.isOn = isFullScreen;
    }

    public void MoveScene(int sceneIndex)
    {
        SceneManager.LoadScene(sceneIndex);
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void OptionsMenu()
    {
        mainMenuHolder.SetActive(false);
        optionMenuHolder.SetActive(true);
        creditMenuHolder.SetActive(false);
        chooseLevelMenuHolder.SetActive(false);

    }

    public void MainMenu()
    {
        mainMenuHolder.SetActive(true);
        optionMenuHolder.SetActive(false);
        creditMenuHolder.SetActive(false);
        chooseLevelMenuHolder.SetActive(false);
    }

    public void CreditMenu()
    {
        mainMenuHolder.SetActive(false);
        optionMenuHolder.SetActive(false);
        creditMenuHolder.SetActive(true);
        chooseLevelMenuHolder.SetActive(false);
    }

    public void ChooseLevelMenu()
    {
        mainMenuHolder.SetActive(false);
        optionMenuHolder.SetActive(false);
        creditMenuHolder.SetActive(false);
        chooseLevelMenuHolder.SetActive(true);
    }

    public void SetScreenResolution(int i)
    {
        if(resolutionToggles[i].isOn)
        {
            activeScreenResIndex = i;
            float aspectRatio = 16 / 9f;
            Screen.SetResolution(screenWidths[i], (int)(screenWidths[i] / aspectRatio), false);
            PlayerPrefs.SetInt("Screen res Index", activeScreenResIndex);
            PlayerPrefs.Save(); 
        }
    }

    public void SetFullScreen(bool isFullScreen)
    {
        for(int i = 0; i<resolutionToggles.Length; i++)
        {
            resolutionToggles[i].interactable = !isFullScreen;
        }

        if(isFullScreen)
        {
            Resolution[] allResolutions = Screen.resolutions;
            Resolution maxResolutions = allResolutions [allResolutions.Length - 1];
            Screen.SetResolution(maxResolutions.width, maxResolutions.height, true);
        
        }
        else
        {
            SetScreenResolution(activeScreenResIndex);
        }
        PlayerPrefs.SetInt("FullScreen", ((isFullScreen) ? 1 : 0));
        PlayerPrefs.Save();
    }
    

    public void SetMasterVolume(float value)
    {
        AudioManager.instance.SetVolume(value, AudioManager.AudioChannel.Master);
    }

    public void SetMusicVolume(float value)
    {
        AudioManager.instance.SetVolume(value, AudioManager.AudioChannel.Music);
    }

    public void SetSfxVolume(float value)
    {
        AudioManager.instance.SetVolume(value, AudioManager.AudioChannel.Sfx);
    }

}
