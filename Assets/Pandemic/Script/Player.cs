﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerController))]
[RequireComponent(typeof(GunController))]
public class Player : LivingEntity
{
    Camera viewCamera;
    const float min = -1.5f;
    const float max = 1.5f;

    public float moveSpeed = 5;

    public Crosshairs crosshairs;
    PlayerController controller;
    GunController gunController;
    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        StartCoroutine(RequestFire());
        
    }

    IEnumerator RequestFire()
    {
        while (true)
        {
            gunController.OnTriggerHold();
            yield return new WaitForSeconds(0.3f);
            gunController.OnTriggerRelease();
        }
    }

    private void Awake()
    {
        controller = GetComponent<PlayerController>();
        gunController = GetComponent<GunController>();
        viewCamera = Camera.main;
        //FindObjectOfType<Spawner>().OnNewWave += OnNewWave;
    }

    public void OnNewWave(int waveNumber)
    {
        health = startingHealth;
        gunController.EquipGun(waveNumber - 1);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 moveInput = new Vector3(SimpleInput.GetAxisRaw("Horizontal"),0, SimpleInput.GetAxisRaw("Vertical"));
        Vector3 moveVelocity = moveInput.normalized * moveSpeed;
        controller.Move(moveVelocity);

        Ray ray = viewCamera.ScreenPointToRay(Input.mousePosition);
        Plane groundPlane = new Plane(Vector3.up,Vector3.up * gunController.GunHeight);
        float rayDistance;

        float xCross = SimpleInput.GetAxisRaw("HorizontalCross");
        float zCross = SimpleInput.GetAxisRaw("VerticalCross");

        if (xCross < max && xCross >min)
        {
            if (zCross < max && zCross>min)
            {
                Vector3 point = new Vector3(transform.position.x + xCross, -0.9f, transform.position.z + zCross);
                Debug.DrawLine(ray.origin, point, Color.red);
                controller.LookAt(point);
                crosshairs.transform.position = point;
                crosshairs.DetectTargets(ray);
            }
        }
        

        //if(groundPlane.Raycast(ray,out rayDistance))
        //{
        //    Vector3 point = ray.GetPoint(rayDistance);
        //    Debug.Log(point);
        //    Debug.DrawLine(ray.origin,point,Color.red);
        //    controller.LookAt(point);
        //    crosshairs.transform.position = point;
        //    crosshairs.DetectTargets(ray);
        //}

        if (Input.GetMouseButton(0))
        {
            gunController.OnTriggerHold();
        }
        if (Input.GetMouseButtonUp(0))
        {
            gunController.OnTriggerRelease();
        }
        if(Input.GetKeyDown(KeyCode.R))
        {
            gunController.Reload();
        }
        if(transform.position.y < -10)
        {
            TakeDamage(health);
        }
    }

    public override void Die()
    {
        AudioManager.instance.PlaySound("Player Death", transform.position);
        base.Die();
    }

}
