﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    [System.Serializable]
    public class Wave
    {
        public int enemyCount;
        public float timeBetweenSpawns;
    }
    public Wave[] waves;
    public EnemyAI enemy;
    Wave currentWave;
    int currentWaveNumber;
    int enemiesRemainingToSpwawn;
    float nextSpawnTime;

    //public Transform[] spawnPoints;
    //public GameObject enemyPrefabs;

    private void Start()
    {
        NextWave();

    }

    private void Update()
    {
        if(enemiesRemainingToSpwawn > 0 && Time.time > nextSpawnTime)
        {
            enemiesRemainingToSpwawn--;
            nextSpawnTime = Time.time + currentWave.timeBetweenSpawns;

            EnemyAI spawnedEnemy = Instantiate(enemy, Vector3.zero, Quaternion.identity) as EnemyAI;
        }
    }

    void NextWave()
    {
        currentWaveNumber++;
        currentWave = waves[currentWaveNumber - 1];
        enemiesRemainingToSpwawn = currentWave.enemyCount;
    }

}
