﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreKeeper : MonoBehaviour
{
    public static int score { get; private set; }
    float lastEnemyKillTime;
    int streakCount;
    float streakExpiryTime = 1;
    public Text streak;

    private void Start()
    {
        score = 0;
        Enemy.OnDeathStatic += OnEnemyKilled;
        FindObjectOfType<Player>().OnDeath += OnPlayerDeath;
    }

    void OnEnemyKilled()
    {
        if(Time.time < lastEnemyKillTime + streakExpiryTime)
        {
            streakCount++;
        }
        else
        {
            streakCount = 0;
        }

        lastEnemyKillTime = Time.time;
        score += 5 + (int)Mathf.Pow(2, streakCount);
        streak.text = "Streak : " + streakCount;
    }

    void OnPlayerDeath()
    {
        Enemy.OnDeathStatic -= OnEnemyKilled;
        PlayerPrefs.SetFloat("YourScore", score);
        if(score > PlayerPrefs.GetFloat("Highscore3")&& score > PlayerPrefs.GetFloat("Highscore2") && score > PlayerPrefs.GetFloat("Highscore"))
        {
            PlayerPrefs.SetFloat("Highscore3", PlayerPrefs.GetFloat("Highscore2"));
            PlayerPrefs.SetFloat("Highscore2", PlayerPrefs.GetFloat("Highscore"));
            PlayerPrefs.SetFloat("Highscore", score);

        }

        if(score > PlayerPrefs.GetFloat("Highscore3") && score > PlayerPrefs.GetFloat("Highscore2") && score < PlayerPrefs.GetFloat("Highscore"))
        {
            PlayerPrefs.SetFloat("Highscore3", PlayerPrefs.GetFloat("Highscore2"));
            PlayerPrefs.SetFloat("Highscore2",score);
            PlayerPrefs.SetFloat("Highscore", PlayerPrefs.GetFloat("Highscore"));
        }

        if(score > PlayerPrefs.GetFloat("Highscore3") && score < PlayerPrefs.GetFloat("Highscore2") && score < PlayerPrefs.GetFloat("Highscore"))
        {
            PlayerPrefs.SetFloat("Highscore3",score);
            PlayerPrefs.SetFloat("Highscore2",PlayerPrefs.GetFloat("Highscore2"));
            PlayerPrefs.SetFloat("Highscore",PlayerPrefs.GetFloat("Highscore"));
        }
    }
}
