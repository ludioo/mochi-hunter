﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuzzleFlash : MonoBehaviour
{
    public GameObject flashHolder;
    public Sprite[] flashSprites;
    public SpriteRenderer[] spriteRenderers;

    public float flashTime;
    // Start is called before the first frame update
    void Start()
    {
        Deactive();
    }

    public void Active()
    {
        flashHolder.SetActive(true);
        int flashSpiteIndex = Random.Range(0, flashSprites.Length);
        for (int i = 0; i < spriteRenderers.Length; i++)
        {
            spriteRenderers[i].sprite = flashSprites[flashSpiteIndex];
        }
        Invoke("Deactive", flashTime);
    }

    void Deactive()
    {
        flashHolder.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
