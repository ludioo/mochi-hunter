﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeSpawn : MonoBehaviour
{
    public GameObject enemy;
    public float spawnTime;
    public float spawnDelay;
    public int currentSpawn;
    public int endSpawn;
    public Transform[] spawnPosition;
    public int score;


    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnObject", spawnTime, spawnDelay);
        score = 0;
    }

    public void SpawnObject()
    {
        Transform currentSpawnPosition = spawnPosition[Random.Range(0, spawnPosition.Length)].transform;
        Instantiate(enemy, currentSpawnPosition.transform.position, transform.rotation);
        currentSpawn ++;
        if(currentSpawn >= endSpawn)
        {
            CancelInvoke("SpawnObject");
        }
    }
    // Update is called once per frame
    void Update()
    {
        if(score == endSpawn)
        {
            Debug.Log("Menang");
        }
        
    }
}
