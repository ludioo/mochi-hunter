﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Sepay.VR {
    public class RaycastCrosshair : MonoBehaviour
    {
        private GameObject raycastObj;

        [Tooltip("Raycast length to detect an object in front of the player")]
        public int rayLength = 10;

        [Tooltip("Interactable LayerMask object")]
        public LayerMask layerMaskInteractable;

        [Tooltip("Crosshair UI to detect the object")]
        public Image crosshairUI;

        private bool highlighted;
        private bool run;

        private void Start()
        {
            run = false;
            highlighted = false;
        }

        // Update is called once per frame
        void Update()
        {
            ButtonToInteract();
        }

        public void ButtonToInteract() {
            RaycastHit hit;
            Vector3 forwardInteract = transform.TransformDirection(Vector3.forward);

            if (Physics.Raycast(transform.position, forwardInteract, out hit, rayLength, layerMaskInteractable.value)) {
                if (hit.collider.CompareTag("Object")) {
                    //hider previus hover
                    if (raycastObj != null && raycastObj.name != hit.collider.gameObject.name) {

                    }
                    raycastObj = hit.collider.gameObject;
                    //show Hover

#if UNITY_ANDROID
                    if (run)
                    {
                        run = false;
                        Debug.Log("YOU INTERACT WITH OBJECT");
                        if (raycastObj.GetComponent<Button>())
                        {
                            raycastObj.GetComponent<Button>().onClick.Invoke();
                        }
                    }
#endif

#if UNITY_STANDALONE
                    if (Input.GetMouseButtonDown(0))
                    {
                        run = false;
                        Debug.Log("YOU INTERACT WITH OBJECT");
                        if (raycastObj.GetComponent<Button>())
                        {
                            raycastObj.GetComponent<Button>().onClick.Invoke();
                        }
                    }
#endif
                }
            }
        }

        public void Clicked()
        {
            run = true;
        }
    }
}
